import string
import random
import numpy
from organisms import Plant, Rabbit
from view import View
#from mingus.midi import fluidsynth
#from mingus.containers.Note import Note
#from mingus.core import scales

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

# The forest has the plants in it
# The list plants has the plant objects
# The ground is a numpy chararray with empy strings as initial values and max length of 6
# Empty spaces are the cells where there is no plant so the value is ''
# So all Plant objects are written to a list, and their names are place on the "map"
class Forest():
    def __init__(self):
        self.grid = {}
        self.entity_store = numpy.zeros((64, 48), dtype='object')
        self.init_grid()
        self.increment_age = numpy.vectorize(lambda x: x.increment_age()) # speedy function application for ageing
        self.num_rabbits = 0
        self.num_plants = 0


# All the empty cells in the forest
    def init_grid(self):
        ground = numpy.zeros((64, 48))
        wh = numpy.where(ground == 0)
        for i in zip(wh[0], wh[1]):
            self.grid[i] = 0
        del ground


    def get_empty_space(self):
        empty_spaces = [x for x in self.grid if self.grid[x] == 0]
        return empty_spaces

    def count_organisms(self):
        num_plants = len([name for name in self.grid.values() if name == 'plant'])
        num_rabbits = len([name for name in self.grid.values() if name == 'rabbit'])
        print num_plants
        print num_rabbits

# The forest can magically create plants
    def create_plants(self, num_plants):
        for i in range(0, num_plants):
            x = Plant("P_"+id_generator())
            pos = (random.randint(0, 10), random.randint(0, 10))
            self.entity_store[pos[0],pos[1]] = x
            self.place_organism(x)
            x.pos = pos
            self.grid[pos] = "plant"

# The forest can also magically create rabbits
    def create_rabbits(self, num_rabbits):
        for i in range(0, num_rabbits):
            x = Rabbit("R_"+id_generator())
            pos = (random.randint(0, 10), random.randint(0, 10))
            self.entity_store[pos[0], pos[1]] = x
            x.pos = pos
            self.grid[pos] = "rabbit"
            x.energy = 30


# Calls their increment_age function + deletes them if they are dead
    def handel_organisms(self):
        organisms = numpy.extract(self.entity_store !=0, self.entity_store)
        self.increment_age(organisms)

        # Loop through the whole grid and check for entities in the numpy array
        for cell in self.grid:
            organism = self.entity_store[cell[0], cell[1]]
            # If organism handle it
            if organism != 0:
                if organism.alive == False:
                    self.grid[organism.pos] = 0
                    self.entity_store[organism.pos[0], organism.pos[1]] = 0

                # If plant do plant stuff
                elif self.grid[cell] == 'plant' and organism.age > 4:
                    new_organism = organism.spawn_plant()
                    # Place newborn plant
                    if new_organism != None:
                        try:
                            pos = self.place_organism(organism)
                            new_organism.pos = pos
                            self.entity_store[pos[0], pos[1]] = new_organism
                        except:
                            pass

                # If Rabbit do rabbit stuff
                elif self.grid[cell] == 'rabbit':
                    new_pos = organism.move(self.grid)
                    if new_pos != 0:
                        self.grid[organism.pos] = 0
                        self.entity_store[organism.pos[0], organism.pos[1]] = 0
                        organism.pos = new_pos
                        self.grid[new_pos] = 'rabbit'
                        self.entity_store[new_pos[0], new_pos[1]] = organism

                    target = organism.eat(self.grid)
                    if target != (0, 0):
                        self.grid[(target[0], target[1])] = 0
                        self.entity_store[target[0], target[1]] = 0

                    if organism.energy > 30:
                        new_organism = organism.procreate()
                        if new_organism != None:

                            try:
                                pos = self.place_organism(organism)
                                new_organism.pos = pos
                                self.grid[pos] = "rabbit"
                                self.entity_store[pos[0], pos[1]] = new_organism
                                new_organism.energy = 30
                            except:
                                #fluidsynth.play_Note(random.randrange(0, 100, 8),0, random.randint(50, 100)) # Play a note
                                pass


# Write the Plant's name in an empty cell
    def place_organism(self, organism):
        organism.get_neighbours(self.grid)
        empty_space = self.get_empty_space()
        neighbours = organism.neighbours
        empty_neighbours = list(set(neighbours).intersection(empty_space))
        random.shuffle(empty_neighbours)
        target = empty_neighbours[0]
        if organism.name[0] == 'P':
            self.grid[(target[0], target[1])] = 'plant'
        else:
            self.grid[(target[0], target[1])] = 'rabbit'
        return (target[0], target[1])



forest = Forest()
turn = 0
forest.create_plants(1)
view = View()

#fluidsynth.init('/usr/share/sounds/sf2/FluidR3_GM.sf2',"alsa")
#scale = scales.dorian("C")

while True:
    turn += 1
    forest.handel_organisms()
    forest.count_organisms()
    view.update_grid(forest.grid)
    #fluidsynth.play_Note(Note(scale[random.randint(0,6)]), 1, random.randint(0,100))
    if turn == 50:
        forest.create_rabbits(2)
    if turn%12 == 0:
        #fluidsynth.set_instrument(1, random.randint(0,100))
        pass

# TODO cuccok aranyatol fuggjon a hang (oktav)
# TODO a jatszott hang befolyasolja a kovetkezo kor valtozoit
