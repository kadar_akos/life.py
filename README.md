### Artificial Life Simulation ###

The goal is to create Plants and Herbivores in a way that none of them die out and sustain a sort of state of equilibrium. Currently the Plants (green) and Rabbits (yellow) are implemented. Rabbits spawn a bit later than the plants and when they start co-existing the system sustains itself. So far so good. The project is also a hipster synthesizer, it plays a note at every iteration from the C dorian scale with a random instrument.

* Dependencies: PyGame, mingus, fluidsynth  
                           sudo pip install pygame  
                           sudo pip install mingus  
                           sudo apt-get install fluidsynth  
  


* Run it with life.py