from random import randint
import numpy

forest_map = numpy.zeros((10, 10))
for i in range(0,10):
    x = randint(0, 9)
    y = randint(0, 9)
    print x, y
    forest_map[x][y] = 1

where= numpy.nonzero(forest_map)
print zip(where[0], where[1])
