import string
import random
import numpy


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Every organism has a boolean for being alive or dead
# They also have a name, age and energy level
class Organism():
    def __init__(self, name):
        self.alive = True
        self.name = name
        self.age = 0
        self.energy = 0
        self.pos = (0, 0)
        self.neighbours = None
        self.alive = True


    def get_neighbours(self, grid):
        x, y = self.pos[0], self.pos[1]
        self.neighbours = [(x-1, y), (x, y-1), (x+1, y), (x, y+1), (x+1, y+1), (x-1, y-1),
                      (x+1, y-1), (x-1, y+1)]
        self.neighbours = [x for x in self.neighbours if x in grid]

    # Increment the age and energy
    def increment_age(self):
        if self.age >= 10:
            self.alive = False
        else:
            self.age = self.age+1
            self.energy = self.energy + 1

    def __str__(self):
        return "%s AGE %s %s ENERGY %s" % (self.name, self.age, self.alive, self.energy)


# Below is the class for the plants, they live happily in the forest
class Plant(Organism):
       # give birth to a new plant
    def spawn_plant(self):
        if self.energy >= 5:
            self.energy = self.energy - 3
            return Plant("P_"+id_generator())
        else:
            pass


class Rabbit(Organism):
    # give birth to a new rabbit
    def procreate(self):
        if self.energy >= 50:
            self.energy = self.energy - 20
            return Rabbit("R_"+id_generator())
        else:
            pass

    # Checks the empty spaces in the forest
    # Chooses a random neighbouring empty cell and returns its indices
    def move(self, grid):
        self.get_neighbours(grid)
        random.shuffle(self.neighbours)
        for i in self.neighbours:
            entity = grid[(i[0], i[1])]
            if entity == 'plant' or entity == 0:
                return i
            else:
                return 0

    def increment_age(self):
        self.age += 1
        self.energy -= 1
        if self.energy <= 0 or self.age >= 70:
            self.alive = False

    def eat(self, grid):
        self.get_neighbours(grid)
        random.shuffle(self.neighbours)
        for i in self.neighbours:
            entity = grid[(i[0], i[1])]
            if entity == 'plant':
                self.energy += 5
                return i
            else:
                return 0, 0