import pygame
import sys
from pygame.locals import *
import random


class Colours:

    def __init__(self):
        # just some colours we want to use
        self.black = (0,  0,  0)
        self.white = (255, 255, 255)
        self.dark_gray = (40, 40, 40)
        self.dark_green = (0, 80, 0)
        self.green = (0, 128, 0)
        self.orange = (255, 165, 0)


class Metrics:

    def __init__(self):
        # just some metrics we want to use
        self.fps = 5
        self.ww = 640  # window (array) width
        self.wh = 480  # window (array) height
        self.cs = 10   # cell size

        # check if we did not fuck up the sizes
        assert self.ww % self.cs == 0, "Window width must be a multiple of cell size"
        assert self.wh % self.cs == 0, "Window height must be a multiple of cell size"

        # number of cells in horizontal and vertical plane
        self.cw = self.ww / self.cs  # number of cells wide
        self.ch = self.wh / self.cs  # number of cells high


class View:

    def __init__(self):
        # set self values, boot pygame, get blank screen with caption
        self.col = Colours()
        self.met = Metrics()
        pygame.init()

        self.fps_clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((self.met.ww, self.met.wh))
        pygame.display.set_caption('Hardcore Squirrels')

        self.screen.fill(self.col.green)  # fill initial screen with green
        self.area = self.blank_grid()     # creates library and populates to match blank grid

    def update_grid(self, m):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        # here we provide a new area, test with random_start_grid
        self.area = m

        # colours the 1 trees, removes the 0 ones
        for item in self.area:
            self.colour_grid(item, self.area, self.screen)

        # shut grid off; self.draw_grid(self.screen)
        pygame.display.update()
        self.fps_clock.tick(self.met.fps)

    def draw_grid(self, screen):
        """
        If preferred, we can add a grid.
        :param screen: the window we dump our shit in
        :return: nothing!
        """
        for x in range(0, self.met.ww, self.met.cs):  # draw vertical lines
            pygame.draw.line(screen, self.col.dark_gray, (x, 0), (x, self.met.wh))
        for y in range(0, self.met.wh, self.met.cs):  # draw horizontal lines
            pygame.draw.line(screen, self.col.dark_gray, (0, y), (self.met.ww, y))

    def colour_grid(self, item, area, screen):
        """
        Colouring function, trees are dark green, dead are green.
        :param item: the particular cell we want to colour code
        :param area: the matrix, ergo bounds, for our forest
        :param screen: the window we dump our shit in
        :return: nothing!
        """
        x = item[0]
        y = item[1]
        y *= self.met.cs  # translates array into grid size
        x *= self.met.cs  # translates array into grid size
        if not area[item] or area[item] == 0:
            pygame.draw.rect(screen, self.col.green, (x, y, self.met.cs, self.met.cs))
        if area[item] == "plant" or area[item] == 1:
            pygame.draw.rect(screen, self.col.dark_green, (x, y, self.met.cs, self.met.cs))
        if area[item] == "rabbit" or area[item] == 2:
            pygame.draw.rect(screen, self.col.orange, (x, y, self.met.cs, self.met.cs))
        return None

    def blank_grid(self):
        """
        creates a dictionary of dead cells, used to initialize,
        might want to correct to array for further implementation
        :return: dictionary that represents the matrix
        """
        grid_dict = {}
        for y in range(self.met.ch):
            for x in range(self.met.cw):
                grid_dict[x, y] = 0  # Sets cells as dead
        return grid_dict

    @staticmethod
    def random_start_grid(area):
        """
        just used to test if our shit works, randomly generates
        beautiful trees in our killer squirrel forest
        :param area: the matrix, ergo bounds, for our forest
        :return: this new area
        """
        for item in area:
            area[item] = random.randint(0, 2)
        return area
#
#g = View()
#while True:
#    g.update_grid(g.random_start_grid(g.area))